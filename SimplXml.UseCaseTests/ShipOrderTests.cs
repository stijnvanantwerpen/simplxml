﻿using System.Linq;
using Crestron.EMEA.SimplUnit;
using Crestron.SimplSharp.CrestronXml;
using SimplXml.Impl;

namespace SimplXml.UseCaseTests
{
    public class ShipOrderTests
    {
        [Fact]
        public void ParseExampleShiporder1()
        {
            var sut = new XmlSerializerImp();
            var reader = new XmlReader(ExampleShiporder1);
            
            //act
            var result = sut.Deserialize<shiporder>(reader);

            result.ShouldNotBeNull();
            result.orderid.ShouldBe("orderid1");
            result.orderperson.ShouldBe("orderperson1");
            result.shipto.ShouldNotBeNull();
            result.shipto.name.ShouldBe("name1");
            result.shipto.address.ShouldBe("address1");
            //...
            result.item.Count().ShouldBe(3);
            result.item[0].title.ShouldBe("title1");
            //...
            result.item[2].price.ShouldBe(decimal.Parse("79228162514264337593543950335"));
        }

        private const string ExampleShiporder1 = @"<?xml version=""1.0"" encoding=""utf-8""?>
            <shiporder orderid=""orderid1"">
              <orderperson>orderperson1</orderperson>
              <shipto>
                <name>name1</name>
                <address>address1</address>
                <city>city1</city>
                <country>country1</country>
              </shipto>
              <item>
                <title>title1</title>
                <note>note1</note>
                <quantity>1</quantity>
                <price>1</price>
              </item>
              <item>
                <title>title2</title>
                <note>note2</note>
                <quantity>79228162514264337593543950335</quantity>
                <price>-79228162514264337593543950335</price>
              </item>
              <item>
                <title>title3</title>
                <note>note3</note>
                <quantity>2</quantity>
                <price>79228162514264337593543950335</price>
              </item>
            </shiporder>";
    }
}