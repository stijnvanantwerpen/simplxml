﻿using System;

namespace SimplXml.Soap
{
    public interface TcpSoapService
    {
        void Connect(string host, ushort port);
        void Send<TRequest, TResponse>(string soapAction, TRequest request, Action<TResponse> handler);
        //void Disconnect();
    }
}