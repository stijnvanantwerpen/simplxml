﻿using System.Collections.Generic;
using SimplXml.Serialization;

namespace SimplXml.Soap
{
    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class SoapEnvelope<TBody>
    {
        public SoapEnvelope()
        {
            Header = new List<object>();
            Body = new List<TBody>();
        }

        [XmlElement(ElementName = "Header", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public IList<object> Header { get; private set; }

        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public IList<TBody> Body { get; private set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class SoapEnvelope : SoapEnvelope<object>
    {
    }
}