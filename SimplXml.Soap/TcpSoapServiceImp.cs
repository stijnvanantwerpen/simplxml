﻿using System;
using System.Linq;
using Crestron.EMEA.I2P.Tools.Hex;
using Crestron.SimplSharp;
using SimplXml.Soap.IoC;
using SimplXml.Soap.Tools.TcpClient;

namespace SimplXml.Soap
{
    public class TcpSoapServiceImp : TcpSoapService, IDisposable
    {       
        private readonly TcpClient _tcpClient;
        private readonly XmlSerializer _xmlSerializer;
        private readonly Factory _factory;
        private readonly SoapUtils _soapUtils;

        public TcpSoapServiceImp(Factory factory)
        {
            _factory = factory;
            _tcpClient = factory.GetTcpClient();            
            _xmlSerializer = factory.GetXmlSerializer();
            _soapUtils = factory.GetSoapUtils();
        }

        public void Connect(string host, ushort port)
        {
            _tcpClient.Connect(host, port);
        }

        private static string GetPayload(ByteString byteStr)
        {
            var dataAsString = byteStr.AsString;
            return dataAsString.Split('\n').Last();            
        }

        public void Send<TRequest,TResponse>(string soapAction, TRequest request, Action<TResponse> handler)
        {
            var soap = _factory.CreateSoapEnvelop();
            soap.Body.Add(request);                        
            var byteArray = _soapUtils.GetRawBytes(soap, soapAction);
            SetupOneShotEventHandler(handler);
            _tcpClient.SendData(byteArray);
        }

        private void SetupOneShotEventHandler<TResponse>(Action<TResponse> handler)
        {
            EventHandler<OnDataReceivedEventArgs> eh = null;
            eh = (sender, e) =>
            {
                var soapXml = GetPayload(e.ReceivedData);
                var response = _xmlSerializer.Deserialize<SoapEnvelope<TResponse>>(soapXml);           
                handler(response.Body.Single());
                _tcpClient.OnDataReceived -= eh;
            };
            _tcpClient.OnDataReceived += eh;
        }

        //public void Disconnect()
        //{
        //    _tcpClient.Disconnect();
        //}

        public void Dispose()
        {
            CrestronConsole.PrintLine("Disposing TcpSoapServiceImp");
            var disposable = _tcpClient as IDisposable;
            if (disposable == null) return;
            disposable.Dispose();
        }
    }
}