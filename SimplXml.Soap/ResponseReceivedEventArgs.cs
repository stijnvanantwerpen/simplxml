﻿using System;

namespace SimplXml.Soap
{
    public class ResponseReceivedEventArgs : EventArgs
    {
        public object Response { get; set; }
    }
}