namespace SimplXml.Soap
{
    public interface SoapUtils
    {
        byte[] GetRawBytes(object obj, string soapAction);
    }
}