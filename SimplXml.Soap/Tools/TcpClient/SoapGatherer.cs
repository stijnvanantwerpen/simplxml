using System;
using System.Linq;
using System.Text;
using Crestron.EMEA.I2P.Tools.Hex;
using Crestron.SimplSharp;

namespace SimplXml.Soap.Tools.TcpClient
{
    public class SoapGatherer
    {
        readonly StringBuilder _sb = new StringBuilder();
        public void Add(ByteString receivedData)
        {
            try
            {
                _sb.Append(receivedData);
                CheckForCompleteSoap();
            }
            catch (Exception ex)
            {
                CrestronConsole.PrintLine("SoapGatherer Exeption: {0}", ex.Message);
            }
        }

        private void CheckForCompleteSoap()
        {
            var msg = _sb.ToString();
            var contenLengthLine = msg.Split('\n').SingleOrDefault(line => line.StartsWith("Content-Length:"));
            if (contenLengthLine == null) return;
            if (msg.Split('\n').All(line => line.Replace("\r", String.Empty) != String.Empty)) return;
            var parts = contenLengthLine.Split(':');
            if (parts.Count() < 2)return;
            var expectedContentLength = Int32.Parse(parts[1]);
            var contentLine = msg.Split('\n').Last();
            if (contentLine.Length < expectedContentLength)return;            
            _sb.Remove(0, _sb.Length);
            CrestronConsole.PrintLine("SoapGatherer: Message received!");
            if (OnSoapReceived == null) return;
            OnSoapReceived(this, new OnSoapReceivedEventArgs
            {
                SoapMessage = msg
            });
        }

        public event EventHandler<OnSoapReceivedEventArgs> OnSoapReceived;
    }
}