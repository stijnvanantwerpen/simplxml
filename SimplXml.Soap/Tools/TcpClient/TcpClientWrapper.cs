﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crestron.EMEA.I2P.Tools.Extensions;
using Crestron.EMEA.I2P.Tools.Hex;
using Crestron.SimplSharp;
using Crestron.SimplSharp.CrestronSockets;

namespace SimplXml.Soap.Tools.TcpClient
{
    public class TcpClientWrapper : TcpClient, IDisposable
    {
        public event EventHandler<OnDataReceivedEventArgs> OnDataReceived;

        private TCPClient _inner;
        private readonly SoapGatherer _gatherer = new SoapGatherer();
        
        private const int TcpBufferSize = 512;
        private const int MsBetweenFrames = 0;
        private const int SocketSendOrReceiveTimeOutInMs = 20000;
     
        private void Initialize(string host, int port)
        {           
            _inner = new TCPClient(new IPEndPoint(IPAddress.Parse(host), port),TcpBufferSize);
            //_inner.SocketSendOrReceiveTimeOutInMs = SocketSendOrReceiveTimeOutInMs;

            _inner.SocketStatusChange += InnerOnSocketStatusChange;
            
            _gatherer.OnSoapReceived += (sender, args) =>
            {
                DisconnectFromServer();

                if (OnDataReceived == null) return;
                OnDataReceived(this, new OnDataReceivedEventArgs
                {
                    ReceivedData = args.SoapMessage
                });
            };
        }

        public void Connect(string host, ushort port)
        {
            CrestronConsole.PrintLine("Connecting....");
            Initialize(host, port);
            var result =_inner.ConnectToServer();            
            if (result != SocketErrorCodes.SOCKET_OK)
            {
                CrestronConsole.PrintLine("TcpClientWrapper: Failed to connect!");
                throw new Exception("Failed to connect!");
            }
            _inner.ReceiveDataAsync(OnDataReceivedCallback);
            CrestronConsole.PrintLine("Connected.");
        }

       

        private void InnerOnSocketStatusChange(TCPClient myTcpClient, SocketStatus clientSocketStatus)
        {
            CrestronConsole.PrintLine("SocketStatusChanged: " + clientSocketStatus.ToString());
        }

        //public void Disconnect()
        //{
        //    CrestronConsole.PrintLine("Disconnecting....");
        //    _inner.DisconnectFromServer();
        //    //CrestronConsole.PrintLine("Disconnected....");
        //}

        private void OnDataReceivedCallback(TCPClient mytcpclient, int numberofbytesreceived)
        {
            CrestronConsole.PrintLine("numberofbytesreceived: " + numberofbytesreceived);
            if(numberofbytesreceived == 0)return;

            try
            {
                var receivedData = _inner.IncomingDataBuffer.Take(numberofbytesreceived).ToArray();
                _inner.ReceiveDataAsync(OnDataReceivedCallback);
                _gatherer.Add(receivedData);
            }
            catch (Exception ex)
            {
                CrestronConsole.PrintLine(ex.Message);
            }
        }

        private CTimer _disconnectAfterSendDataTimer ;

        public void SendData(ByteString data)
        {
            CrestronConsole.PrintLine("Start sending data.");
            if (_disconnectAfterSendDataTimer != null)
            {
                CrestronConsole.PrintLine("TcpClientWrapper.SendData() _disconnectAfterSendDataTimer is not null");
                throw new Exception("_disconnectAfterSendDataTimer is not null");
            }

            var frames = data.AsBytes.Chunk(TcpBufferSize).Select(bytes => bytes.ToArray());
            foreach (var frame in frames)
            {
                CrestronConsole.PrintLine("Sending data {0}....", frame.Length);
                _inner.SendData(frame, frame.Length);
                CrestronEnvironment.Sleep(MsBetweenFrames);
            }
            CrestronConsole.PrintLine("Sending data completed");

            _disconnectAfterSendDataTimer = new CTimer(specific =>
            {
                CrestronConsole.PrintLine("_disconnectAfterSendDataTimer Elapsed");
                DisconnectFromServer();
            }, SocketSendOrReceiveTimeOutInMs);
        }

        private void DisconnectFromServer()
        {
            CrestronConsole.PrintLine("TcpClientWrapper.DisconnectFromServer()");
            if (_disconnectAfterSendDataTimer != null)
            {
                _disconnectAfterSendDataTimer.Stop();
                _disconnectAfterSendDataTimer.Dispose();
            }
            if (_inner == null) return;
            _inner.DisconnectFromServer();            
        }

        public void Dispose()
        {
            CrestronConsole.PrintLine("Disposing TcpClientWrapper...");
            DisconnectFromServer();

            var disposable = _inner as IDisposable;
            if (disposable == null) return;
            disposable.Dispose();
        }
    }

    public class OnDataReceivedEventArgs : EventArgs
    {
        public ByteString ReceivedData { get; set; }
    }

    public class OnSoapReceivedEventArgs : EventArgs
    {
        public string SoapMessage { get; set; }
    }
}