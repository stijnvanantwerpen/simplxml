﻿using System;
using Crestron.EMEA.I2P.Tools.Hex;

namespace SimplXml.Soap.Tools.TcpClient
{
    public interface TcpClient
    {
        event EventHandler<OnDataReceivedEventArgs> OnDataReceived;

        void SendData(ByteString data);
        void Connect(string host, ushort port);
        //void Disconnect();
    }
}