﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.EMEA.I2P.Tools.Hex;
using Crestron.SimplSharp;
using Crestron.SimplSharp.Net.Http;

namespace SimplXml.Soap.Tools.TcpClient
{
    public class HttpClientWrapper : TcpClient
    {
        private readonly HttpClient _inner = new HttpClient();

        public event EventHandler<OnDataReceivedEventArgs> OnDataReceived;
        public void SendData(ByteString data)
        {
            var request = new HttpClientRequest
            {
                ContentBytes = data.AsBytes,
                Url = new UrlParser("http://10.32.4.100:905/loewe_tablet_0001"),
                RequestType = RequestType.Post
            };
            _inner.DispatchAsync(request, (userobj, error) =>
            {
                if(OnDataReceived == null)return;
                OnDataReceived(this, new OnDataReceivedEventArgs
                {
                    ReceivedData = userobj.ContentBytes
                });
            });
        }

        public void Connect(string host, ushort port)
        {
            _inner.Connect(host, port);
        }

        public void Disconnect()
        {            
        }
    }
}