using System;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using Crestron.EMEA.I2P.Tools.Hex;
using Crestron.SimplSharp.CrestronIO;
using Crestron.SimplSharp.CrestronXml;
using SimplXml.Impl;

namespace SimplXml.Soap
{
    public class SoapUtilsImp : SoapUtils
    {
        private readonly XmlSerializer _xmlSerializer = new XmlSerializerImp();

        public byte[] GetRawBytes(object obj, string soapAction)
        {            
            var xmlDoc = SerializeAndLoadXmlDocument(obj);
            ByteString content = xmlDoc.DocumentElement.OuterXml;
            var bytes = content.AsBytes;            
            var tcpHeader = GetTcpHeader(bytes, soapAction);
            var byteArray = tcpHeader.AsBytes.Concat(bytes).ToArray();
            return byteArray;
        }

        private XmlDocument SerializeAndLoadXmlDocument(object obj)
        {
            var xmlDoc = new XmlDocument();
            using (var stream = new MemoryStream())
            {
                _xmlSerializer.Serialize(stream, obj);

                stream.Flush();
                stream.Seek(0, SeekOrigin.Begin);

                xmlDoc.Load(stream);
                return xmlDoc;
            }
        }

        private ByteString GetTcpHeader(ByteString content, string soapAction)
        {
            var sb = new StringBuilder();
            sb.AppendLine("POST /loewe_tablet_0001 HTTP/1.1");
            //sb.AppendLine("Accept-Encoding: gzip,deflate");
            sb.AppendLine("Content-Type: text/xml;charset=UTF-8");
            sb.AppendLine(String.Format(@"SOAPAction: ""urn:loewe.de:RemoteTV:Tablet#{0}""", soapAction));
            sb.AppendLine(String.Format("Content-Length: {0}", content.AsString.Length));
            sb.AppendLine("Host: 10.32.4.100:905");
            //sb.AppendLine("Connection: Keep-Alive");
            //sb.AppendLine("User-Agent: Apache-HttpClient/4.1.1 (java 1.5)");            
            sb.AppendLine("");
            //sb.Replace("\r\n", "\n");
            return sb.ToString();
        }
    }
}