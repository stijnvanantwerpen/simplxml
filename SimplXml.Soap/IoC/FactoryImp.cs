﻿using SimplXml.Impl;
using SimplXml.Soap.Tools.TcpClient;

namespace SimplXml.Soap.IoC
{
    public class FactoryImp : Factory
    {
        public TcpClient GetTcpClient()
        {
            return new TcpClientWrapper();
            //return new HttpClientWrapper();
        }

        public XmlSerializer GetXmlSerializer()
        {
            return new XmlSerializerImp();
        }

        public SoapEnvelope CreateSoapEnvelop()
        {
            return new SoapEnvelope();
        }

        public SoapEnvelope<TResponse> CreateSoapEnvelop<TResponse>()
        {
            return new SoapEnvelope<TResponse>();
        }    

        public SoapUtils GetSoapUtils()
        {
            return new SoapUtilsImp();
        }
    }
}