﻿using Crestron.SimplSharp.CrestronIO;
using SimplXml.Soap.Tools.TcpClient;

namespace SimplXml.Soap.IoC
{
    public interface Factory
    {
        TcpClient GetTcpClient();
        XmlSerializer GetXmlSerializer();
        SoapEnvelope CreateSoapEnvelop();
        SoapEnvelope<TResponse> CreateSoapEnvelop<TResponse>();
        SoapUtils GetSoapUtils();
    }
}