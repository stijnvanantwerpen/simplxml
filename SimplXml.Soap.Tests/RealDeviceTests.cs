﻿using System;
using System.Linq;
using Crestron.EMEA.SimplUnit;
using Crestron.SimplSharp;
using SimplXml.Soap.IoC;
using SimplXml.Soap.Tools.TcpClient;

namespace SimplXml.Soap.Tests
{
    public class RealDeviceTests
    {
        [Fact(Skip = "Test on real device")]
        public void SendRequestAccessToLoeweTv()
        {
            var onDataReceivedWasCalled = false;
            var utils = new SoapUtilsImp();

            var soapEnvelope = new SoapEnvelope<RequestAccessRequestType>();
            var requestAccessRequestType = new RequestAccessRequestType
            {
                ClientId = "?",
                DeviceName = "Dell_54:b0:86",
                DeviceType = "Dell",
                DeviceUUID = "F8:CA:B8:54:B0:00",
                RequesterName = "Crestron Demo App",
                fcid = "8138941"
            };
            //soapEnvelope.Body.SetValue(requestAccessRequestType);
            soapEnvelope.Body.Add(requestAccessRequestType);

            var request = utils.GetRawBytes(soapEnvelope, "RequestAccess");

            var sut = new TcpClientWrapper();
            sut.Connect("10.32.4.100",905);
            try
            {
                sut.OnDataReceived += (sender, args) =>
                {
                    onDataReceivedWasCalled = true;
                    args.ReceivedData.AsString.Any().ShouldBe(true);
                };
                sut.SendData(request);

                CrestronEnvironment.Sleep(3000);
            }
            finally
            {
                CrestronConsole.PrintLine("Test: Finally");
                onDataReceivedWasCalled.ShouldBe(true);
            }
        }

        [Fact (Skip="Test on real device")]
        public void SendRequestAccess()
        {
            var onResponseReceivedWasCalled = false;
            var requestAccessRequestType = new RequestAccessRequestType
            {
                ClientId = "?",
                DeviceName = "Dell_54:b0:86",
                DeviceType = "Dell",
                DeviceUUID = "F8:CA:B8:54:B0:00",
                RequesterName = "Crestron Demo App",
                fcid = "8138941"
            };
            var sut = new TcpSoapServiceImp(new FactoryImp());

            try
            {
                sut.Connect("10.32.4.100", 905);
                sut.Send<RequestAccessRequestType, RequestAccessResponseType>("RequestAccess", requestAccessRequestType, (response) =>
                {
                    onResponseReceivedWasCalled = true;
                    response.ClientId.ShouldNotBe("");
                });
                CrestronEnvironment.Sleep(5000);
            }
            finally
            {
                CrestronConsole.PrintLine("Test: Finally");
            }
            onResponseReceivedWasCalled.ShouldBe(true);
        }
    }
}