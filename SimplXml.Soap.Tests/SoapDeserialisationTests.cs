﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.EMEA.SimplUnit;
using Crestron.SimplSharp;
using SimplXml.Tests;

namespace SimplXml.Soap.Tests
{
    public class SoapDeserialisationTests
    {
        [Fact]
        public void DeserilizeFromSoapEnvelopeRequestAccessResponseType()
        {
            const string responseXml = @"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/1999/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/1999/XMLSchema"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""> <SOAP-ENV:Header/> <SOAP-ENV:Body>  <m:RequestAccessResponse xmlns:m=""urn:loewe.de:RemoteTV:Tablet"">  <m:fcid>Unknown Device</m:fcid><m:ClientId>LRemoteClient-6-1484142456</m:ClientId><m:AccessStatus>Accepted</m:AccessStatus></m:RequestAccessResponse> </SOAP-ENV:Body></SOAP-ENV:Envelope>";

            var response = TestHelper.DeserilizeFrom<SoapEnvelope<RequestAccessResponseType>>(responseXml);
            //response.Body.GetValue().ClientId.ShouldBe("LRemoteClient-6-1484142456");
            response.Body.Single().ClientId.ShouldBe("LRemoteClient-6-1484142456");
        }
    }
}