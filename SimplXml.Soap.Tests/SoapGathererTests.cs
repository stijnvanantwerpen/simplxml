﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.EMEA.I2P.Tools.Extensions;
using Crestron.EMEA.SimplUnit;
using Crestron.SimplSharp;
using SimplXml.Soap.Tools.TcpClient;

namespace SimplXml.Soap.Tests
{
    public class SoapGathererTests
    {
        [Fact]
        public void ReceiveSoapMessage()
        {
            var data = @"HTTP/1.1 200 OK
Date: Tue, 17 Jan 2017 09:24:49 GMT
Server: Nano HTTPD library
Content-Type: text/xml
Content-Length: 532

<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/1999/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/1999/XMLSchema"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""> <SOAP-ENV:Header/> <SOAP-ENV:Body>  <m:RequestAccessResponse xmlns:m=""urn:loewe.de:RemoteTV:Tablet"">  <m:fcid>Unknown Device</m:fcid><m:ClientId>LRemoteClient-0-1484641489</m:ClientId><m:AccessStatus>Pending</m:AccessStatus></m:RequestAccessResponse> </SOAP-ENV:Body></SOAP-ENV:Envelope>";

            var onSoapReceivedWasCalled = false;
            var sut = new SoapGatherer();
            sut.OnSoapReceived += (sender, e) =>
            {
                onSoapReceivedWasCalled = true;
                e.SoapMessage.ShouldNotBeNull();
            };
            
            sut.Add(data);

            onSoapReceivedWasCalled.ShouldBe(true);
        }

        [Fact]
        public void ReceiveSoapMessageInFrames()
        {
            var frames = @"HTTP/1.1 200 OK
Date: Tue, 17 Jan 2017 09:24:49 GMT
Server: Nano HTTPD library
Content-Type: text/xml
Content-Length: 532

<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/1999/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/1999/XMLSchema"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""> <SOAP-ENV:Header/> <SOAP-ENV:Body>  <m:RequestAccessResponse xmlns:m=""urn:loewe.de:RemoteTV:Tablet"">  <m:fcid>Unknown Device</m:fcid><m:ClientId>LRemoteClient-0-1484641489</m:ClientId><m:AccessStatus>Pending</m:AccessStatus></m:RequestAccessResponse> </SOAP-ENV:Body></SOAP-ENV:Envelope>"
                .Chunk(36).Select(chars => new string(chars.ToArray())).ToArray();           

            var onSoapReceivedWasCalled = false;
            var sut = new SoapGatherer();
            sut.OnSoapReceived += (sender, e) =>
            {
                onSoapReceivedWasCalled = true;
                e.SoapMessage.ShouldNotBeNull();
            };

            for(var i = 0; i<frames.Count() -1; i++)
            {
                var frame = frames.ElementAt(i);
                sut.Add(frame);
                onSoapReceivedWasCalled.ShouldBe(false);
            }
            sut.Add(frames.Last());
            onSoapReceivedWasCalled.ShouldBe(true);
        }
    }
}