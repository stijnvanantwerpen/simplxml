/// <opmerkingen/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
//[SimplXml.Serialization.XmlTypeAttribute(Namespace="urn:loewe.de:RemoteTV:Tablet")]
[SimplXml.Serialization.XmlRoot(ElementName = "RequestAccess", Namespace = "urn:loewe.de:RemoteTV:Tablet")]
public partial class RequestAccessRequestType : SyncRequestType {
    
    private string clientIdField;
    
    private string deviceTypeField;
    
    private string deviceNameField;
    
    private string deviceUUIDField;
    
    private string requesterNameField;
    
    /// <opmerkingen/>
    [SimplXml.Serialization.XmlElementAttribute(Form=SimplXml.Schema.XmlSchemaForm.Unqualified)]
    public string ClientId {
        get {
            return this.clientIdField;
        }
        set {
            this.clientIdField = value;
        }
    }
    
    /// <opmerkingen/>
    [SimplXml.Serialization.XmlElementAttribute(Form=SimplXml.Schema.XmlSchemaForm.Unqualified)]
    public string DeviceType {
        get {
            return this.deviceTypeField;
        }
        set {
            this.deviceTypeField = value;
        }
    }
    
    /// <opmerkingen/>
    [SimplXml.Serialization.XmlElementAttribute(Form=SimplXml.Schema.XmlSchemaForm.Unqualified)]
    public string DeviceName {
        get {
            return this.deviceNameField;
        }
        set {
            this.deviceNameField = value;
        }
    }
    
    /// <opmerkingen/>
    [SimplXml.Serialization.XmlElementAttribute(Form=SimplXml.Schema.XmlSchemaForm.Unqualified)]
    public string DeviceUUID {
        get {
            return this.deviceUUIDField;
        }
        set {
            this.deviceUUIDField = value;
        }
    }
    
    /// <opmerkingen/>
    [SimplXml.Serialization.XmlElementAttribute(Form=SimplXml.Schema.XmlSchemaForm.Unqualified)]
    public string RequesterName {
        get {
            return this.requesterNameField;
        }
        set {
            this.requesterNameField = value;
        }
    }
}