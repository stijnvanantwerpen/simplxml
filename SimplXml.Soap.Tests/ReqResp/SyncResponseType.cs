/// <opmerkingen/>
[SimplXml.Serialization.XmlIncludeAttribute(typeof(RequestAccessResponseType))]
/*[SimplXml.Serialization.XmlIncludeAttribute(typeof(ClientResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(ParentalLockResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(SetSettingResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetSettingsResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(PlayMultiroomResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetVolumesResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetDRPlusArchiveResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetListOfVirtualListsResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(AddFavoriteListFromVirtualResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(ExportServiceListsResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(ImportServiceListsResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(MoveServicesFavoriteListResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(RemoveServicesFavoriteListResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(AddServicesFavoriteListResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(RenameFavoriteListResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(RemoveFavoriteListResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(AddFavoriteListResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetListOfChannelListsResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetFeatureResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(SetActionFieldResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(InjectKeyboardKeyResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(InjectRCKeyResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(PlayerSeekToResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(PlayerSetSpeedResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(PlayerStopResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(PlayerResumeResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(PlayerPauseResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(PlayerPlayResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(PlayerReleasePreparedResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(PlayerPrepareResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(ZapToMediaResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(ZapToApplicationResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetMediaEventResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetMediaItemResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetChannelListResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(SetValueToBrowserJSResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetBrowserJSChangeListResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetValueFromBrowserJSResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetCurrentPlaybackResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(ProgramTimerResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(StandbyResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(MuteResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(VolumeResponseType))]*/
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[SimplXml.Serialization.XmlRoot(ElementName = "Sync", Namespace = "urn:loewe.de:RemoteTV:Tablet")]
public partial class SyncResponseType {
    
    private string fcidField;
    
    /// <opmerkingen/>
    [SimplXml.Serialization.XmlElementAttribute(Form=SimplXml.Schema.XmlSchemaForm.Unqualified)]
    public string fcid {
        get {
            return this.fcidField;
        }
        set {
            this.fcidField = value;
        }
    }
}