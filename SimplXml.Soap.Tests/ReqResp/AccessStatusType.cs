/// <opmerkingen/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.SerializableAttribute()]
[SimplXml.Serialization.XmlRoot(ElementName = "AccessStatus", Namespace = "urn:loewe.de:RemoteTV:Tablet")]
public enum AccessStatusType {
    
    /// <opmerkingen/>
    Pending,
    
    /// <opmerkingen/>
    Denied,
    
    /// <opmerkingen/>
    Accepted,
}