/// <opmerkingen/>
[SimplXml.Serialization.XmlIncludeAttribute(typeof(RequestAccessRequestType))]
/*
[SimplXml.Serialization.XmlIncludeAttribute(typeof(ClientRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(ParentalLockRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(SetSettingRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetSettingsRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(PlayMultiroomRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetVolumesRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetDRPlusArchiveRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetListOfVirtualListsRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(AddFavoriteListFromVirtualRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(ExportServiceListsRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(ImportServiceListsRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(MoveServicesFavoriteListRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(RemoveServicesFavoriteListRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(AddServicesFavoriteListRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(RenameFavoriteListRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(RemoveFavoriteListRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(AddFavoriteListRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetListOfChannelListsRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetFeatureRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(SetActionFieldRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(InjectKeyboardKeyRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(InjectRCKeyRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(PlayerSeekToType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(PlayerSetSpeedType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(PlayerStopType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(PlayerResumeType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(PlayerPauseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(PlayerPlayType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(PlayerReleasePreparedType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(PlayerPrepareType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(ZapToMediaRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(ZapToApplicationRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetNowNextEventResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetNowNextEventRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetMediaEventRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetMediaItemRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetChannelListRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(SetValueToBrowserJSRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetBrowserJSChangeListRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetValueFromBrowserJSRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetDeviceDataResponseType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(ProgramTimerRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(StandbyRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(SetMuteRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetMuteRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(SetVolumeRequestType))]
[SimplXml.Serialization.XmlIncludeAttribute(typeof(GetVolumeRequestType))]*/
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[SimplXml.Serialization.XmlRoot(ElementName = "Sync", Namespace = "urn:loewe.de:RemoteTV:Tablet")]
public partial class SyncRequestType {
    
    private string fcidField;
    
    /// <opmerkingen/>
    //[SimplXml.Serialization.XmlElementAttribute(Form=SimplXml.Schema.XmlSchemaForm.Unqualified)]
    public string fcid {
        get {
            return this.fcidField;
        }
        set {
            this.fcidField = value;
        }
    }
}