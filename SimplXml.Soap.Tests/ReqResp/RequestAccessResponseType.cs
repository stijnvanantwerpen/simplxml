/// <opmerkingen/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[SimplXml.Serialization.XmlRoot(ElementName = "RequestAccess", Namespace = "urn:loewe.de:RemoteTV:Tablet")]
public partial class RequestAccessResponseType : SyncResponseType {
    
    private string clientIdField;
    
    private AccessStatusType accessStatusField;
    
    /// <opmerkingen/>
    [SimplXml.Serialization.XmlElementAttribute(Form=SimplXml.Schema.XmlSchemaForm.Unqualified)]
    public string ClientId {
        get {
            return this.clientIdField;
        }
        set {
            this.clientIdField = value;
        }
    }
    
    /// <opmerkingen/>
    [SimplXml.Serialization.XmlElementAttribute(Form=SimplXml.Schema.XmlSchemaForm.Unqualified)]
    public AccessStatusType AccessStatus {
        get {
            return this.accessStatusField;
        }
        set {
            this.accessStatusField = value;
        }
    }
}