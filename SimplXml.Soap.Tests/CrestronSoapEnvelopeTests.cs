﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.EMEA.SimplUnit;
using Crestron.SimplSharp;
using Crestron.SimplSharp.CrestronXml;
using SimplXml.Impl;
using SimplXml.Tests.TestObjects;

namespace SimplXml.Soap.Tests
{
    public class CrestronSoapEnvelopeTests
    {
        [Fact]
        public void CreateSoapEnvelope()
        {
            var sut = new SoapEnvelope();

            var xml = Serialize(sut);

            xml.ShouldNotBe(String.Empty);
            xml.Contains(@"<Envelope xmlns=""http://schemas.xmlsoap.org/soap/envelope/""><Header /><Body /></Envelope>").ShouldBe(true);
        }

        [Fact]
        public void DeserializeSoapEnvelope()
        {
            var xml = Deserialize<SoapEnvelope>(@"<Envelope xmlns=""http://schemas.xmlsoap.org/soap/envelope/""><Header /><Body /></Envelope>");

            xml.ShouldNotBeNull();            
        }

        [Fact]
        public void CreateSoapEnvelopeWithBody()
        {
            var sut = new SoapEnvelope();
            sut.Body.Add("Hello World!");

            var xml = Serialize(sut);

            xml.ShouldNotBe(String.Empty);
            xml.Contains(@"<Body><String>Hello World!</String></Body>").ShouldBe(true);
        }

        [Fact]
        public void DeserializeSoapEnvelopeWithBody()
        {
            var xml = Deserialize<SoapEnvelope<String>>(@"<Envelope xmlns=""http://schemas.xmlsoap.org/soap/envelope/""><Header /><Body><String>Hello World!</String></Body></Envelope>");

            xml.ShouldNotBeNull();
            xml.Body.ShouldNotBeNull();
            xml.Body.First().ShouldBe("Hello World!");          
            //xml.Body.GetValue().ShouldBe("Hello World!");          
        }

        [Fact]
        public void CreateSoapEnvelopeWithComplexBody()
        {
            var sut = new SoapEnvelope();
            sut.Body.Add(new MyDog { Years = 14 });
            sut.Body.Add(new MyCat { Lives = 9 });

            var xml = Serialize(sut);
            CrestronConsole.PrintLine(xml);

            xml.ShouldNotBe(String.Empty);
            xml.Contains(@"<MyDog><Years>14</Years><Sound>Woef Woef</Sound></MyDog>").ShouldBe(true);
            xml.Contains(@"<MyCat><Lives>9</Lives><Sound>Miauw</Sound></MyCat>").ShouldBe(true);
        }

        [Fact]
        public void CreateSoapEnvelopeWithArray()
        {
            var sut = new SoapEnvelope();
            //var animals = new MyAnimal[]{ 
            var dog = new MyDog {Years = 14};
            var cat = new MyCat { Lives = 9 };
            sut.Body.Add(dog);
            sut.Body.Add(cat);


            var xml = Serialize(sut);
            CrestronConsole.PrintLine(xml);

            xml.ShouldNotBe(String.Empty);
            xml.Contains(@"<MyDog><Years>14</Years><Sound>Woef Woef</Sound></MyDog>").ShouldBe(true);
            xml.Contains(@"<MyCat><Lives>9</Lives><Sound>Miauw</Sound></MyCat>").ShouldBe(true);
        }

        [Fact]
        public void CreateSoapEnvelopeWithList()
        {
            var sut = new SoapEnvelope();
            var animals = new List<MyAnimal>{ 
                new MyDog { Years = 14 },
                new MyCat { Lives = 9 }};
            sut.Body.Add(animals);

            var xml = Serialize(sut);
            CrestronConsole.PrintLine(xml);

            xml.ShouldNotBe(String.Empty);
            xml.Contains(@"<MyDog><Years>14</Years><Sound>Woef Woef</Sound></MyDog>").ShouldBe(true);
            xml.Contains(@"<MyCat><Lives>9</Lives><Sound>Miauw</Sound></MyCat>").ShouldBe(true);
        }


        [Fact]
        public void SerializeSoapEnvelopeWithRequestAccessResponseType()
        {
            var soapEnvelope = new SoapEnvelope<RequestAccessRequestType>();
            var requestAccessRequestType = new RequestAccessRequestType
            {
                ClientId = "?",
                DeviceType = "Dell",
                DeviceName = "Dell_54:b0:86",
                DeviceUUID = "F8:CA:B8:54:B0:00",
                RequesterName = "Crestron Demo App",
                fcid = "8138941"
            };

            soapEnvelope.Body.Add(requestAccessRequestType);
            //soapEnvelope.Body.SetValue(requestAccessRequestType);
            var xml = Serialize(soapEnvelope);

            const string soapEnvelopXml = @"<?xml version=""1.0"" encoding=""utf-16""?><Envelope xmlns=""http://schemas.xmlsoap.org/soap/envelope/""><Header /><Body><RequestAccess xmlns=""urn:loewe.de:RemoteTV:Tablet""><ClientId>?</ClientId><DeviceType>Dell</DeviceType><DeviceName>Dell_54:b0:86</DeviceName><DeviceUUID>F8:CA:B8:54:B0:00</DeviceUUID><RequesterName>Crestron Demo App</RequesterName><fcid>8138941</fcid></RequestAccess></Body></Envelope>";
            xml.ShouldBe(soapEnvelopXml);
        }

        [Fact]
        public void DeserializeSoapEnvelopeWithRequestAccessResponseType()
        {
            const string soapEnvelopXml = @"<Envelope xmlns=""http://schemas.xmlsoap.org/soap/envelope/""><Header /><Body><RequestAccess xmlns=""urn:loewe.de:RemoteTV:Tablet""><ClientId>?</ClientId><DeviceType>Dell</DeviceType><DeviceName>Dell_54:b0:86</DeviceName><DeviceUUID>F8:CA:B8:54:B0:00</DeviceUUID><RequesterName>Crestron Demo App</RequesterName><fcid>8138941</fcid></RequestAccess></Body></Envelope>";
            var soapEnvelope = Deserialize<SoapEnvelope<RequestAccessRequestType>>(soapEnvelopXml);

            var value = soapEnvelope.Body.Single();
            //var value = soapEnvelope.Body.GetValue();
            value.ClientId.ShouldBe("?");
        }


        private static string Serialize(object sut)
        {
            var sb = new StringBuilder();
            using (var writer = new XmlWriter(sb))
            {
                new XmlSerializerImp().Serialize(writer, sut);
                writer.Flush();
                writer.Close();
            }
            var xml = sb.ToString();
            return xml;
        }

        private static TTarget Deserialize<TTarget>(string xml) where TTarget : class, new()
        {
            return new XmlSerializerImp().Deserialize<TTarget>(xml);
        }
    }
}