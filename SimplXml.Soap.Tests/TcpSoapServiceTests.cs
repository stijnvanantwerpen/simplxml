﻿using Crestron.EMEA.I2P.Tools.Hex;
using Crestron.EMEA.SimplUnit;
using SimplXml.Soap.Tests.Mocks;
using SimplXml.Soap.Tests.ReqResp;
using SimplXml.Soap.Tools.TcpClient;

namespace SimplXml.Soap.Tests
{  
    public class TcpSoapServiceTests
    {
        [Fact]
        public void TheSerialisedObjectShouldBeSendAsData()
        {
            //Arrange
            var request = new HelloWorldRequest();
            var soapEnvelop = new SoapEnvelope();            
            ByteString serialisedVersionOfTheRequest = "This is the Xml of the Serialised request";
            const string soapAction = "soapAction";
            var bytes = new byte[12];

            var mockFactory = new MockFactory();
            var mockTcpClient = new MockTcpClient();
            var mockXmlSerializer = new MockXmlSerializer();
            var mockSoapUtil = new MockSoapUtils();
            
            mockFactory.Expect(e => e.GetTcpClient()).Return(mockTcpClient).Once();
            mockFactory.Expect(e => e.GetXmlSerializer()).Return(mockXmlSerializer).Once();
            mockFactory.Expect(e => e.CreateSoapEnvelop()).Return(soapEnvelop).Once();
            mockFactory.Expect(e => e.GetSoapUtils()).Return(mockSoapUtil).Once();

            mockSoapUtil.Expect(e => e.GetRawBytes(soapEnvelop, soapAction)).Return(bytes);
            mockXmlSerializer
                .Expect(e => e.Serialize(bytes)).Return(serialisedVersionOfTheRequest).Once();
            mockTcpClient.Expect(e => e.SendData(bytes.AsByteString())).Once();

            //Act
            var sut = new TcpSoapServiceImp(mockFactory);
            sut.Send<HelloWorldRequest, HelloWorldResponse>(soapAction,request, null);            
            
            //Assert
            mockFactory.VerifyAllExpectations();
            mockTcpClient.VerifyAllExpectations();
        }       
    }
}