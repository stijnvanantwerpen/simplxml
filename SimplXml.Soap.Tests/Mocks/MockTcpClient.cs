using System;
using Crestron.EMEA.I2P.Tools.Hex;
using Crestron.EMEA.SimplUnit;
using Crestron.EMEA.SimplUnit.SimplMock;
using SimplXml.Soap.Tools.TcpClient;

namespace SimplXml.Soap.Tests.Mocks
{
    public class MockTcpClient : BaseMock<TcpClientInvoker>, TcpClient
    {
        public event EventHandler<OnDataReceivedEventArgs> OnDataReceived;

        public void SendData(ByteString data)
        {
            Do(i => i.SendData(data));
        }

        public void Connect(string host, ushort port)
        {
            Do(i => i.Connect(host, port));
        }

        public void Disconnect()
        {
            Do(i => i.Disconnect());
        }

        public EventInvoker<OnDataReceivedEventArgs> GetOnDateReceived()
        {
            return GetEvent(OnDataReceived);
        }
    }

    public class TcpClientInvoker : Invoker
    {
        public ExpectationVoid SendData(Arg<ByteString> data)
        {
            return GetExpectationFor("SendData", data);
        }

        public ExpectationVoid Connect(Arg<string> host, Arg<ushort> port)
        {
            return GetExpectationFor("Connect", host, port);
        }

        public EventInvoker<OnDataReceivedEventArgs> OnDataReceived {
            get { return ((MockTcpClient) Mock).GetOnDateReceived(); }
        }

        public ExpectationVoid Disconnect()
        {
            return GetExpectationFor("Disconnect");
        }
    }
}