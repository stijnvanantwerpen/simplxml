﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.EMEA.SimplUnit;
using Crestron.EMEA.SimplUnit.SimplMock;

namespace SimplXml.Soap.Tests.Mocks
{
    public class MockSoapUtils : BaseMock<SoapUtilsInvoker>,  SoapUtils
    {                
        public byte[] GetRawBytes(SoapEnvelope<object> soap)
        {
            return Do(i => i.GetRawBytes(soap));
        }

        public byte[] GetRawBytes(object soap, string soapAction)
        {
            return Do(i => i.GetRawBytes(soap, soapAction));
        }
    }

    public class SoapUtilsInvoker : Invoker
    {
        public Expectation<byte[]> GetRawBytes(Arg<object> soap)
        {
            return GetExpectationFor<byte[]>("GetRawBytes", soap);
        }

        public Expectation<byte[]> GetRawBytes(Arg<object> soap, Arg<string> soapAction)
        {
            return GetExpectationFor<byte[]>("GetRawBytes", soap, soapAction);
        }
    }
}