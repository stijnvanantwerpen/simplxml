﻿using System;
using Crestron.EMEA.SimplUnit.SimplMock;
using SimplXml.Soap.IoC;
using SimplXml.Soap.Tools.TcpClient;

namespace SimplXml.Soap.Tests.Mocks
{
    public class MockFactory : BaseMock<FactoryInvoker>, Factory
    {
        public MockFactory(): base(new FactoryInvoker())
        {
        }
        
        public TcpClient GetTcpClient()
        {
            return Do(i => i.GetTcpClient());
        }

        public XmlSerializer GetXmlSerializer()
        {
            return Do(i => i.GetXmlSerializer());
        }

        public SoapEnvelope CreateSoapEnvelop()
        {
            return Do(i => i.CreateSoapEnvelop());
        }

        public SoapEnvelope<TResponse> CreateSoapEnvelop<TResponse>()
        {           
            return Do(i => i.CreateSoapEnvelop<TResponse>());
        }

        public SoapUtils GetSoapUtils()
        {
            return Do(i => i.GetSoapUtils());
        }
    }

    public class FactoryInvoker : Invoker
    {
        public Expectation<TcpClient> GetTcpClient()
        {
            return GetExpectationFor<TcpClient>("GetTcpClient");
        }

        public Expectation<XmlSerializer> GetXmlSerializer()
        {
            return GetExpectationFor<XmlSerializer>("GetXmlSerializer");
        }

        public Expectation<SoapEnvelope> CreateSoapEnvelop()
        {
            return GetExpectationFor<SoapEnvelope>("CreateSoapEnvelop");
        }

        public Expectation<SoapEnvelope<TResponse>> CreateSoapEnvelop<TResponse>()
        {
            return GetExpectationFor<SoapEnvelope<TResponse>>(String.Format("CreateSoapEnvelop<{0}>", typeof(TResponse).Name));
        }

        public Expectation<SoapUtils> GetSoapUtils()
        {
            return GetExpectationFor<SoapUtils>("GetSoapUtil");
        }
    }
}