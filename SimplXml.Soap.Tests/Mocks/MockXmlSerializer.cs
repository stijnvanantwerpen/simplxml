﻿using System.Collections.Generic;
using Crestron.EMEA.I2P.Tools.Hex;
using Crestron.EMEA.SimplUnit;
using Crestron.EMEA.SimplUnit.SimplMock;
using Crestron.SimplSharp.CrestronIO;
using Crestron.SimplSharp.CrestronXml;

namespace SimplXml.Soap.Tests.Mocks
{
    public class MockXmlSerializer : BaseMock<XmlSerializerInvoker>, XmlSerializer
    {
        public MockXmlSerializer() : base(new XmlSerializerInvoker())
        {
        }

        public void Serialize(Stream stream, object obj)
        {
            Do(i => i.Serialize(stream, obj));
        }

        public void Serialize(XmlWriter writer, object obj)
        {
            Do(i => i.Serialize(writer, obj));
        }

        public TResult Deserialize<TResult>(XmlReader reader) where TResult : class, new()
        {
            return Do(i => i.Deserialize<TResult>(reader));
        }

        public IEnumerable<byte> Serialize(object obj)
        {
            return Do(i => i.Serialize(obj));
        }

        public TResult Deserialize<TResult>(ByteString byteString) where TResult : class, new()
        {
            return Do(i => i.Deserialize<TResult>(byteString));
        }
    }

    public class XmlSerializerInvoker : Invoker
    {
        public ExpectationVoid Serialize(Arg<Stream> stream, Arg<object> o)
        {
            return GetExpectationFor("Serialize", stream, o);
        }

        public ExpectationVoid Serialize(Arg<XmlWriter> writer, Arg<object> o)
        {
            return GetExpectationFor("Serialize", writer, o);
        }

        public Expectation<T> Deserialize<T>(XmlReader reader)
        {
            return GetExpectationFor<T>("Deserialize", reader);
        }

        public Expectation<byte[]> Serialize(Arg<object> request)
        {
            return GetExpectationFor<byte[]>("Serialize", request);
        }

        public Expectation<T> Deserialize<T>(Arg<ByteString> byteString)
        {
            return GetExpectationFor<T>("Deserialize", byteString);
        }
    }
}