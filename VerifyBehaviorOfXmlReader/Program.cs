﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Soap;
using SimplXml.Tests.TestObjects;

namespace VerifyBehaviorOfXmlReader
{
    class Program
    {
        static void Main(string[] args)
        {
            //ReadElementContent();
            //SerializeACat();
            //SerializeSoapEnvelop.SerializeASoapEnvelop();
            //SerializeString.HelloWorld();
            //SerializeString.HelloInt();
            SerializeASoapEnvelop();

            Console.ReadLine();
        }

        private static void SerializeASoapEnvelop()
        {
            var envelop = new Envelope();

            envelop.Body = new Body();


            var xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(Envelope));

            var sb = new StringBuilder();
            var xmlWriter = XmlWriter.Create(sb);
            xmlSerializer.Serialize(xmlWriter, envelop);
            var xml = sb.ToString();
            Console.WriteLine(xml);
        }

        private static void SerializeACat()
        {
            var hello = new HelloInheritance
            {
                Animal = new MyCat {Lives = 7}
            };
            var xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(HelloInheritance));

            var sb = new StringBuilder();
            var xmlWriter = XmlWriter.Create(sb);
            xmlSerializer.Serialize(xmlWriter, hello);
            var xml = sb.ToString();
        }

        private static void ReadElementContent()
        {
            const string xml =
                @"<?xml version=""1.0"" encoding=""utf-8""?><root><element1>value1</element1><element2>value2</element2></root>";
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(xml);
            writer.Flush();
            stream.Seek(0, SeekOrigin.Begin);
            var reader = XmlReader.Create(stream);
            reader.ReadToFollowing("root");
            reader.ReadStartElement();
            Console.WriteLine(reader.LocalName); //element1
            var value = reader.ReadElementContentAsString();
            Console.WriteLine(reader.LocalName); //element2, while it still should be element1
            Console.ReadLine();
        }
    }
}
