﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;


namespace SimplXml.Tests.TestObjects
{    
    public class HelloInheritance
    {
        public MyAnimal Animal { get; set; }
    }

    public class HelloInheritanceList
    {        
        public IEnumerable<MyAnimal> Animals { get; set; }
    }

    [XmlInclude(typeof(MyCat))]
    [XmlInclude(typeof(MyDog))]
    public abstract class MyAnimal
    {
        public String Sound
        {
            get { return GetMyText(); }
        }

        protected virtual string GetMyText()
        {
            return "You should never see this";
        }
    }

    public class MyDog : MyAnimal
    {
        public int Years;

        protected override string GetMyText()
        {
            return "Woef Woef";
        }
    }

    public class MyCat : MyAnimal
    {
        public int Lives;

        protected override string GetMyText()
        {
            return "Miauw";
        }
    }
}