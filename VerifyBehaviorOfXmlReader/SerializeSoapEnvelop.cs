﻿using System;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace VerifyBehaviorOfXmlReader
{
    class SerializeSoapEnvelop
    {
        public static void SerializeASoapEnvelop()
        {
            var envelope = new SimplXml.Soap.SoapEnvelope();
            var sb = new StringBuilder();
            var writer = XmlWriter.Create(sb);
            new XmlSerializer(typeof(SimplXml.Soap.SoapEnvelope)).Serialize(writer, envelope);
            writer.Close();
            var xml = sb.ToString();
            Console.WriteLine(xml);
        }        
    }
}
