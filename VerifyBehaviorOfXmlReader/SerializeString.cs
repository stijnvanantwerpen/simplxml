﻿using System;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace VerifyBehaviorOfXmlReader
{
    class SerializeString
    {
        public static void HelloWorld()
        {
            var sb = new StringBuilder();
            var writer = XmlWriter.Create(sb);
            new XmlSerializer(typeof(String)).Serialize(writer, "Hello World!!");
            writer.Close();
            var xml = sb.ToString();
            Console.WriteLine(xml);
        }

        public static void HelloInt()
        {
            var sb = new StringBuilder();
            var writer = XmlWriter.Create(sb);
            new XmlSerializer(typeof(int)).Serialize(writer, 42);
            writer.Close();
            var xml = sb.ToString();
            Console.WriteLine(xml);
        }
    }
}
