﻿using System.Collections.Generic;
using Crestron.EMEA.I2P.Tools.Hex;
using Crestron.SimplSharp.CrestronIO;
using Crestron.SimplSharp.CrestronXml;

namespace SimplXml
{
    public interface XmlSerializer
    {
        void Serialize(Stream stream, object obj);
        void Serialize(XmlWriter writer, object obj);
        TResult Deserialize<TResult>(XmlReader reader) where TResult : class, new();
        
        //Added this one since it is quiete handy
        IEnumerable<byte> Serialize(object obj);
        TResult Deserialize<TResult>(ByteString reader) where TResult : class, new();
        
    }
}