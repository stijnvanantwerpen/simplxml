﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
using System;
using Crestron.SimplSharp.Reflection;

namespace SimplXml.Tools
{
    public static class MemberInfoExtensions
    {     
        public static object GetValue(this MemberInfo self, object obj, object[] index)
        {
            if (self == null) throw new ArgumentNullException("self");
            if (self is FieldInfo) return (self as FieldInfo).GetValue(obj);
            if (self is PropertyInfo) return (self as PropertyInfo).GetValue(obj, index);            
            throw new ArgumentException(String.Format("GetValue() not possible for <{0}>", self.GetType().Name));
        }

        public static void SetValue(this MemberInfo self, object obj, object value, object[] index)
        {
            if (self == null) throw new ArgumentNullException("self");
            if (self is FieldInfo) (self as FieldInfo).SetValue(obj, value);
            else if (self is PropertyInfo) (self as PropertyInfo).SetValue(obj, value, index);
            
            else throw new ArgumentException(String.Format("SetValue() not possible for <{0}>", self.GetType().Name));
        }

        public static Type GetMemberType(this MemberInfo self)
        {
            if (self == null) throw new ArgumentNullException("self");
            
            if (self is FieldInfo) return (self as FieldInfo).FieldType;
            if (self is PropertyInfo) return (self as PropertyInfo).PropertyType;
            if (self is MethodInfo) return (self as MethodInfo).ReturnType;
            if (self is ConstructorInfo) return (self as ConstructorInfo).ReflectedType ;
            throw new ArgumentException(String.Format("GetMemberType() not possible for <{0}>", self.GetType().Name));
        }
    }
}