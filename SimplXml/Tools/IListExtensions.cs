﻿using System;
using System.Collections;
using System.Linq;

namespace SimplXml.Tools
{
    public static class IListExtensions
    {
        public static Type GetElementType(this IList self)
        {
            return self.GetType().GetGenericArguments().FirstOrDefault()
                   ?? self.GetType().GetElementType();
        }
    }
}