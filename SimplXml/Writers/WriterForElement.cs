﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using Crestron.SimplSharp.CrestronXml;

namespace SimplXml.Writers
{
    public class WriterForElement : SubNodeWriter
    {
        public string Name { get; set; }
        public string Namespace { get; set; }
        public int Order { get; set; }       
       
        private readonly List<NodeWriter> _nodeWriters = new List<NodeWriter>(); 
        
        protected virtual void WriteBegin(XmlWriter writer)
        {
            var listName = XmlConvert.EncodeName(Name);
            writer.WriteStartElement(listName, Namespace);           
        }

        public void WriteInner(XmlWriter writer)
        {
            foreach (var nodeWriter in _nodeWriters.OfType<WriterForAttribute>().OrderBy(a => a.Name))
            {
                nodeWriter.Write(writer);
            }
            foreach (var nodeWriter in _nodeWriters.OfType<SubNodeWriter>().OrderBy(OrderByOrderWithUndefinedLast()))
            {
                nodeWriter.Write(writer);
            }
        }

        private static Func<SubNodeWriter, int> OrderByOrderWithUndefinedLast()
        {
            return x => (x.Order == -1)? Int32.MaxValue : x.Order;
        }


        protected virtual void WriteEnd(XmlWriter writer)
        {
            writer.WriteEndElement();
        }

        public virtual void Write(XmlWriter writer)
        {
            WriteBegin(writer);
            WriteInner(writer);
            WriteEnd(writer);
        }

        public void Add(NodeWriter nodeWriter)
        {
            _nodeWriters.Add(nodeWriter);
        }

        public void Add(IEnumerable<NodeWriter> nodeWriters)
        {
            foreach (var nodeWriter in nodeWriters)
            {
                _nodeWriters.Add(nodeWriter);
            }
        }
    }
}