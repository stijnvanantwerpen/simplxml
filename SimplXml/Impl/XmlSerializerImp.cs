﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

using System.Collections.Generic;
using Crestron.EMEA.I2P.Tools.Hex;
using Crestron.SimplSharp.CrestronIO;
using Crestron.SimplSharp.CrestronXml;

namespace SimplXml.Impl
{
    public class XmlSerializerImp : XmlSerializer
    {
        private readonly XmlDeserializeStrategy _xmlDeserializeStrategy = new XmlDeserializeStrategy();
        private readonly XmlSerializeStrategy _xmlSerializeStrategy = new XmlSerializeStrategy();

        public void Serialize(Stream stream, object obj)
        {
            _xmlSerializeStrategy.Serialize(stream, obj);
        }

        public void Serialize(XmlWriter writer, object obj)
        {
            _xmlSerializeStrategy.Serialize(writer, obj);
        }

        public IEnumerable<byte> Serialize(object obj)
        {
            return _xmlSerializeStrategy.Serialize(obj);
        }

        public TResult Deserialize<TResult>(ByteString byteString) where TResult : class, new()
        {
            return _xmlDeserializeStrategy.Deserialize<TResult>(byteString);
        }

        public TResult Deserialize<TResult>(XmlReader reader) where TResult : class, new()
        {
            return _xmlDeserializeStrategy.Deserialize<TResult>(reader);
        }
    }
}
