using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp.CrestronIO;
using Crestron.SimplSharp.CrestronXml;
using Crestron.SimplSharp.Reflection;
using SimplXml.Serialization;
using SimplXml.Tools;
using SimplXml.Writers;

namespace SimplXml.Impl
{
    class XmlSerializeStrategy
    {
        public void Serialize(Stream stream, object obj)
        {
            using (var writer = new XmlWriter(stream, new XmlWriterSettings { Encoding = Encoding.GetEncoding("Latin1") }))
            {
                Serialize(writer, obj);
                writer.Flush();
                writer.Close();
            }
        }

        public void Serialize(XmlWriter writer, object obj)
        {
            var rootWriter = BuildRootWriter((CType)obj.GetType());
            rootWriter.Add(GetValueWriters(obj));
            rootWriter.Write(writer);
        }

        public IEnumerable<byte> Serialize(object obj)
        {
            using (var memStream = new MemoryStream())
            {                                
                Serialize(memStream, obj);
                return memStream.ToArray();
            }
        }

        private static WriterForElement BuildRootWriter(MemberInfo type)
        {
            var rootAttribute = type.GetCustomAttributes(typeof(XmlRootAttribute), true)
                .SingleOrDefault() as XmlRootAttribute
                                ?? new XmlRootAttribute();
            var rootWriter = new WriterForElement
            {
                Name = rootAttribute.ElementName.OrWhenEmpty(type.Name),
                Namespace = rootAttribute.Namespace
            };          
            return rootWriter;
        }

        private static IEnumerable<NodeWriter> GetChildWriters(object parent)
        {
            if (parent == null) yield break;            

            CType type = parent.GetType();
    
            var props = type.GetProperties();
            var fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
            var fieldsAnProperties = fields.Union<MemberInfo>(props);

            foreach (var memberInfo in fieldsAnProperties)
            {
                yield return GetNodeWriter(parent, memberInfo);
            }            
        }

        private static NodeWriter GetNodeWriter(object parent, MemberInfo memberInfo)
        {
            return
                GetNodeWriterForIgnored(memberInfo)
                ?? GetNodeWriterForAttribute(parent, memberInfo)
                ?? GetNodeWriterForElement(parent, memberInfo);
        }

        private static NodeWriter GetNodeWriterForIgnored(ICustomAttributeProvider property)
        {
            return property.GetCustomAttributes(typeof(XmlIgnoreAttribute), true).Any() ? new WriterForNullWriter() : null;
        }

        private static NodeWriter GetNodeWriterForAttribute(object parent, MemberInfo property)
        {
            var attributeAttr =
                property.GetCustomAttributes(typeof(XmlAttributeAttribute), true).SingleOrDefault() as
                    XmlAttributeAttribute;

            if (attributeAttr == null) return null;
            return new WriterForAttribute
            {
                Name = attributeAttr.AttributeName.OrWhenEmpty(property.Name),
                Namespace = attributeAttr.Namespace,
                Value = property.GetValue(parent, null)
            };
        }

        private static WriterForElement GetNodeWriterForElement(object parent, MemberInfo property)
        {
            var elementWriter = GetPropertyWriterAsElementWriter_GetElementWriter(property);
            var valueWriters = GetValueWriters(property.GetValue(parent, null));
            var xsiTypeWriter = GetXsiTypeWriter(parent, property);

            elementWriter.Add(valueWriters);
            elementWriter.Add(xsiTypeWriter);
            return elementWriter;
        }

        private static NodeWriter GetXsiTypeWriter(object parent, MemberInfo property)
        {
            if (!property.GetMemberType().GetCustomAttributes(typeof(XmlIncludeAttribute), true).Any())
                return new WriterForNullWriter();

            var xsiTypeWriter = new WriterForAttribute
            {
                Name = "type",
                Namespace = "http://www.w3.org/2001/XMLSchema-instance",
                Value = property.GetValue(parent, null).GetType().Name
            };
            return xsiTypeWriter;
        }

        private static WriterForElement GetPropertyWriterAsElementWriter_GetElementWriter(MemberInfo property)
        {
            var elementAttr = property.GetCustomAttributes(typeof(XmlElementAttribute), true).SingleOrDefault()
                as XmlElementAttribute ?? new XmlElementAttribute();
            return new WriterForElement
            {
                Name = elementAttr.ElementName.OrWhenEmpty(property.Name),
                Namespace = elementAttr.Namespace,
                Order = elementAttr.Order
            };
        }

        private static IEnumerable<NodeWriter> GetValueWriters(object value)
        {
            if (value is IEnumerable && !(value is string))
            {
                return GetValueWriters_ForIEnumerable(value as IEnumerable);
            }
            if (value is Enum)
            {
                return GetValueWriters_ForEnum(value as Enum);
            }
            return GetValueWriters_ForSingleObject(value);
        }

        private static IEnumerable<NodeWriter> GetValueWriters_ForEnum(Enum value)
        {
            CType type = value.GetType();
            var enumValuField = type.GetMember(value.ToString()).FirstOrDefault();
            var enumAttr = enumValuField.GetCustomAttributes(typeof(XmlEnumAttribute), true).SingleOrDefault() as XmlEnumAttribute ?? new XmlEnumAttribute();

            yield return new WriterForEnum { Value = enumAttr.Name.OrWhenEmpty(value.ToString()) };
        }

        private static IEnumerable<NodeWriter> GetValueWriters_ForIEnumerable(IEnumerable items)
        {
            foreach (var item in items)
            {
                var itemAttr = item.GetType().GetCustomAttributes(typeof(XmlRootAttribute), true)
                    .SingleOrDefault() as XmlRootAttribute ?? new XmlRootAttribute();
                var writer = new WriterForEnumerable
                {
                    Name = itemAttr.ElementName.OrWhenEmpty(item.GetType().Name),
                    Namespace = itemAttr.Namespace
                };                
                writer.Add(GetValueWriters(item));
                yield return writer;
            }
        }

        private static IEnumerable<NodeWriter> GetValueWriters_ForSingleObject(object obj)
        {
            if (obj == null) yield break;

     
                CType type = obj.GetType();
                if (type.IsValueType || obj is string)
                {
                    yield return
                        new WriterForValue
                        {
                            Value = obj
                        };
                }
                else
                {
                    foreach (var writer in GetChildWriters(obj))
                    {
                        yield return writer;
                    }
                }

        }
    }
}