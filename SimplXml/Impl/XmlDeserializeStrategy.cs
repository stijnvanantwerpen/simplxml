using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Crestron.EMEA.I2P.Tools.Extensions;
using Crestron.EMEA.I2P.Tools.Hex;
using Crestron.SimplSharp.CrestronXml;
using Crestron.SimplSharp.Reflection;
using SimplXml.Serialization;
using SimplXml.Tools;
using Activator = Crestron.SimplSharp.Reflection.Activator;

namespace SimplXml.Impl
{
    class XmlDeserializeStrategy
    {
        public TResult Deserialize<TResult>(ByteString byteString) where TResult : class, new()
        {
            using (var reader = new XmlReader(byteString.AsBytes))
            {
                return Deserialize<TResult>(reader);
            }
        }

        public TResult Deserialize<TResult>(XmlReader reader) where TResult : class, new()
        {
            var result = Activator.CreateInstance<TResult>();
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(reader);
            ReadAttributes(xmlDoc.DocumentElement, result);
            ReadChildElements(xmlDoc.DocumentElement, result);
            return result;
        }

        private static void ReadChildElements(XmlNode xmlElement, object result)
        {
            CType type = result.GetType();
            foreach (var nodeGroup in xmlElement.GetChildNodes().GroupBy(node => node.Name))
            {
                if (nodeGroup.Count() == 1)
                {
                    var node = (XmlElement)nodeGroup.Single();
                    var property = GetPropertyForElement(node.LocalName, type);
                    if(property == null) continue;
  
                    ReadChildElements_ReadElement(node, result, property);                                        

                }
                else
                {
                    var node = nodeGroup.First();
                    var property = GetPropertyForElement(node.LocalName, type);
                    if(property == null)continue;

                    var elementType = property.GetMemberType().GetElementType();
                    var list = Array.CreateInstance(elementType, nodeGroup.Count());
                    ReadChildElements_ReadList(nodeGroup, list);
                    property.SetValue(result, list, null);
                }
            }
        }

        private static void ReadChildElements_ReadElement(XmlElement node, object result, MemberInfo property)
        {
            if (node.IsEmpty && !node.HasAttributes)
            {
                //when <element />
                //property will remain default (null for reference types)
                return;
            }
            if (String.IsNullOrEmpty(node.InnerXml) && !node.HasAttributes)
            {
                //when <element></element>
                //property will be set to empty value
                property.SetValue(result, String.Empty, null);
                return;
            }
            var obj = MakeObjectFor(node, property.GetMemberType());
            ReadAttributes(node, obj);
            property.SetValue(result, obj, null);
        }

        private static object MakeObjectFor(XmlNode childNode, CType type)
        {
            

            if (type.IsValueType || type == typeof(string))
            {
                return MakeObjectFor_MakeValue(childNode, type);
            }
            if (typeof(IEnumerable).IsAssignableFrom(type))
            {
                return MakeObjectFor_MakeList(childNode.ChildNodes, type);
            }
            return MakeObjectFor_MakeObject(childNode, type);
        }

        private static object MakeObjectFor_MakeObject(XmlNode childNode, Type type)
        {
            var attr = GetXmlIncludeFor(type, childNode);
            var obj = Activator.CreateInstance(attr.Type);            
            ReadChildElements(childNode, obj);

            var node = childNode as XmlElement;
            ReadAttributes(node, obj);
            return obj;
        }

        private static XmlIncludeAttribute GetXmlIncludeFor(Type type, XmlNode node)
        {
            var xsiTypeAttr = node.Attributes.GetNamedItem("type", "http://www.w3.org/2001/XMLSchema-instance");
            var xsiType = (xsiTypeAttr == null) ? null : xsiTypeAttr.Value;

            return type.GetCustomAttributes(typeof(XmlIncludeAttribute), true)
                .SingleOrDefault(attr => ((XmlIncludeAttribute)attr).Type.Name == xsiType) as XmlIncludeAttribute ??
                   new XmlIncludeAttribute(type);
        }

        private static object MakeObjectFor_MakeList(XmlNodeList nodeList, Type type)
        {
            var list = ReadChildElements_MakeListFor(type, nodeList.Count);
            ReadChildElements_ReadList(nodeList, list);
            return list;
        }

        private static object MakeObjectFor_MakeValue(XmlNode childNode, Type type)
        {
            var valueAsString = childNode.HasChildNodes ? childNode.FirstChild.Value: String.Empty;
            return type.IsEnum 
                ? valueAsString.ParseAs(type) 
                : Convert.ChangeType(valueAsString, type, null);
        }

        private static IList ReadChildElements_MakeListFor(Type typeOfList, int count)
        {

            var genericType = typeOfList.GetGenericArguments().FirstOrDefault();
            if (genericType != null)
            {
                var list = (IList) Activator.CreateInstance(typeof (List<>).MakeGenericType(genericType));
                for (var i = 0; i < count; i++)
                {
                    var obj = genericType.IsAbstract || genericType == typeof(string) ? null : Activator.CreateInstance(genericType);
                    list.Add(obj);
                }
                return list;
            }


            var arrayElementType = typeOfList.GetElementType();
            return Array.CreateInstance(arrayElementType, count);
        }

        private static void ReadChildElements_ReadList(XmlNodeList nodeList, IList list)
        {
            ReadChildElements_ReadList(nodeList.AsEnumerable(), list);
        }

        private static void ReadChildElements_ReadList(IEnumerable<XmlNode> nodeList, IList list)
        {
            var i = 0;
            var type = list.GetElementType();
            foreach (var obj in nodeList.Select(node => MakeObjectFor(node, type)))
            {
                list[i++] =obj;
            }
        }

        private static void ReadAttributes<TResult>(XmlElement element, TResult result)
        {
            if (element == null) return;
            if (!element.HasAttributes) return;            
            foreach (var attribute in element.GetAttributes())
            {
                var property = GetPropertyForAttribute(attribute.LocalName, result.GetType());
                if (property == null) continue;
                var value = Convert.ChangeType(attribute.Value, property.PropertyType, null);
                property.SetValue(result, value, null);
            }
        }

        private static MemberInfo GetPropertyForElement(string elementName, CType parrentType)
        {
            return parrentType.GetMembers().SingleOrDefault(p =>                
                IsPropertyFor(elementName, p)) ;
        }

        private static bool IsPropertyFor(string elementName, MemberInfo p)
        {
            return IsPropertyFor_NeverForMethodsOrConstructors(p)                   
                   ?? IsPropertyFor_(elementName, p);
        }

        private static bool IsPropertyFor_(string elementName, MemberInfo p)
        {
            return (p.GetCustomAttributes(typeof (XmlElementAttribute), true).SingleOrDefault()
                as XmlElementAttribute ?? new XmlElementAttribute()).ElementName.OrWhenEmpty(p.Name) ==
                   elementName;
        }
       

        private static bool? IsPropertyFor_NeverForMethodsOrConstructors(MemberInfo p)
        {
            if (p is MethodInfo) return false;
            if (p is ConstructorInfo) return false;
            return null;
        }

        private static PropertyInfo GetPropertyForAttribute(string attributeName, CType parrentType)
        {
            return parrentType.GetProperties().SingleOrDefault(p =>
                (p.GetCustomAttributes(typeof(XmlAttributeAttribute), true).SingleOrDefault() as
                    XmlAttributeAttribute ?? new XmlAttributeAttribute()).AttributeName.OrWhenEmpty(p.Name) ==
                attributeName);
        }
    }
}