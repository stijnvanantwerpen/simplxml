//------------------------------------------------------------------------------
// <copyright file="XmlRootAttribute.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
// <owner current="true" primary="true">Microsoft</owner>                                                                
//------------------------------------------------------------------------------
//
// Only the namespace has been changed.

using System;

namespace SimplXml.Serialization
{
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
	public class XmlIgnoreAttribute : Attribute
	{
	}
}
