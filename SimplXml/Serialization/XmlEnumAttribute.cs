//------------------------------------------------------------------------------
// <copyright file="XmlRootAttribute.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
// <owner current="true" primary="true">Microsoft</owner>                                                                
//------------------------------------------------------------------------------
//
// Only the namespace has been changed.

using System;

namespace SimplXml.Serialization
{
	[AttributeUsage(AttributeTargets.Field)]
	public class XmlEnumAttribute : Attribute
	{
		private string name;

		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		public XmlEnumAttribute()
		{
		}

		public XmlEnumAttribute(string name)
		{
			this.name = name;
		}
	}
}
