﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
using System;
using System.Text;
using Crestron.EMEA.SimplUnit;
using Crestron.SimplSharp.CrestronXml;
using SimplXml.Tools;

namespace SimplXml.Tests.ExtensionTests
{
    public class XmlElementExtensionsTests
    {
        [Fact]
        public void GetAttributesEnumerator()
        {
            const string xmlString = @"<?xml version=""1.0"" encoding=""iso-8859-1""?><root attr1='hello' attr2='world'></root>";
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);
            var root = xmlDoc.DocumentElement;
            
            var attributecounter = 0;
            var sb = new StringBuilder();
            foreach (var attribute in root.GetAttributes())
            {
                attribute.ShouldNotBeNull();
                attributecounter++;
                attribute.Name.ShouldBe(String.Format("attr{0}", attributecounter));
                sb.Append(attribute.Value);
            }
            attributecounter.ShouldBe(2);
            sb.ToString().ShouldBe("helloworld");
        }

        [Fact]
        public void GetChildNodesEnumerator()
        {
            const string xmlString = @"<?xml version=""1.0"" encoding=""iso-8859-1""?><root><list><element>hello</element><element>world</element></list></root>";
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);
            var root = xmlDoc.DocumentElement;

            var nodecounter = 0;
            var sb = new StringBuilder();
            var listNode = root.ChildNodes[0] as XmlElement;
            foreach (var node in listNode.GetChildNodes())
            {
                node.ShouldNotBeNull();
                nodecounter++;
                node.Name.ShouldBe(String.Format("element", nodecounter));
                sb.Append(node.FirstChild.Value);
            }
            nodecounter.ShouldBe(2);
            sb.ToString().ShouldBe("helloworld");
        }
    }
}