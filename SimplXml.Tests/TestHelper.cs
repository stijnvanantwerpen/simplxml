﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
using Crestron.SimplSharp.CrestronIO;
using Crestron.SimplSharp.CrestronXml;
using SimplXml.Impl;

namespace SimplXml.Tests
{
    public class TestHelper
    {
        public static XmlDocument SerializeAndLoadXmlDocument(object obj)
        {
            var xmlDoc = new XmlDocument();
            var stream = new MemoryStream();

            var sut = new XmlSerializerImp();

            sut.Serialize(stream, obj);

            stream.Flush();
            stream.Seek(0, SeekOrigin.Begin);

            xmlDoc.Load(stream);
            return xmlDoc;
        }

        public static TResult DeserilizeFrom<TResult>(string str) where TResult : class, new()
        {
            TResult result;
            using (var stream = new MemoryStream())
            {
                var writer = new StreamWriter(stream);
                writer.Write(str);
                writer.Flush();
                stream.Seek(0, SeekOrigin.Begin);

                var reader = new XmlReader(stream);

                var sut = new XmlSerializerImp();
                result = sut.Deserialize<TResult>(reader);
                writer.Close();
                reader.Close();
            }
            return result;
        }
    }
}