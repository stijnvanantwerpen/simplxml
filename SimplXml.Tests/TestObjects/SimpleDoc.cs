﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
using SimplXml.Serialization;

namespace SimplXml.Tests.TestObjects
{
    [XmlRoot(Namespace = "http://crestron.eu/myNamespace")]
    public class SimpleDoc
    {
        [XmlAttribute(Namespace = "http://crestron.eu/myNamespaceForAttributes")]
        public string Attr1 { get; set; }        

        [XmlElement(Order=3, ElementName="LastElement", Namespace = "")]
        public string ElementZero { get; set; }

        [XmlElement(Order=1, Namespace = "http://crestron.eu/myNamespace")]
        public string FirstElement { get; set; }

        [XmlElement(Order=2, Namespace = "http://crestron.eu/myNamespace2")]
        public string SecondElement { get; set; }

        [XmlAttribute(AttributeName = "Attr3", Namespace = "http://crestron.eu/myNamespaceForAttributes")]
        public string Attr2 { get; set; }

        [XmlElement(Order = 4)]
        public int NoNamespaceSet { get; set; }
    }
}
