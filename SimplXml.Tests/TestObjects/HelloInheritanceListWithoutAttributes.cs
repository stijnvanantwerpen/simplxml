﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
using System;
using System.Collections.Generic;

namespace SimplXml.Tests.TestObjects
{    
    public class HelloInheritanceSingleWithoutAttributes
    {
        public Animal Animal { get; set; }
    }

    public class HelloInheritanceListWithoutAttributes
    {        
        public IEnumerable<Animal> Animals { get; set; }
    }

    public abstract class Animal
    {
        public String Sound
        {
            get { return GetMyText(); }
        }

        protected virtual string GetMyText()
        {
            return "You should never see this";
        }
    }

    public class Dog : Animal
    {
        public int Years;

        protected override string GetMyText()
        {
            return "Woef Woef";
        }
    }

    public class Cat : Animal
    {
        public int Lives;

        protected override string GetMyText()
        {
            return "Miauw";
        }
    }
}