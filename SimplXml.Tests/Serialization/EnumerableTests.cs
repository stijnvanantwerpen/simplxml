﻿using System.Collections.Generic;
using Crestron.EMEA.SimplUnit;
using SimplXml.Tests.TestObjects;

namespace SimplXml.Tests.Serialization
{
    public class EnumerableTests
    {
        [Fact]
        public void SerializeAnimalList()
        {
            var animals = new List<MyAnimal>{ 
                new MyDog { Years = 14 },
                new MyCat { Lives = 9 }};

            var xmlDoc = TestHelper.SerializeAndLoadXmlDocument(animals);

            xmlDoc.ShouldNotBeNull();
        }


        [Fact]
        public void SerializeAnimalArray()
        {
            var animals = new MyAnimal[]{ 
                new MyDog { Years = 14 },
                new MyCat { Lives = 9 }};

            var xmlDoc = TestHelper.SerializeAndLoadXmlDocument(animals);

            xmlDoc.ShouldNotBeNull();
        }
    }
}