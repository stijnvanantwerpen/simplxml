﻿using Crestron.EMEA.SimplUnit;

namespace SimplXml.Tests.Serialization
{
    public class SerializeStringTests
    {
        [Fact]
        public void SerializeSingleString()
        {
            const string sut = "Hello World!!";
            var xmlDoc = TestHelper.SerializeAndLoadXmlDocument(sut);

            xmlDoc.ShouldNotBeNull();
            xmlDoc.OuterXml.ShouldBe(@"<?xml version=""1.0"" encoding=""iso-8859-1""?><String>Hello World!!</String>");
        }

        [Fact]
        public void SerializeAnInteger()
        {
            const int sut = 42;
            var xmlDoc = TestHelper.SerializeAndLoadXmlDocument(sut);

            xmlDoc.ShouldNotBeNull();
            xmlDoc.OuterXml.ShouldBe(@"<?xml version=""1.0"" encoding=""iso-8859-1""?><Int32>42</Int32>");
        }
    }
}