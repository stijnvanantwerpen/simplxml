﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
using System;
using Crestron.EMEA.SimplUnit;
using Crestron.SimplSharp;
using Crestron.SimplSharp.CrestronXml;
using SimplXml.Tests.TestObjects;

namespace SimplXml.Tests.Serialization
{
    public class XmlSerializerTests
    {
        [Fact]
        public void HelloWorld0()
        {
            var obj = new HelloWorld0();

            var xmlDoc = TestHelper.SerializeAndLoadXmlDocument(obj);

            xmlDoc.InnerXml.ShouldBe(@"<?xml version=""1.0"" encoding=""iso-8859-1""?><HelloWorld0 />");
        }

        [Fact]
        public void HelloWorld1()
        {
            try
            {

                var obj = new HelloWorld1 {HelloWorld = "Hello World!"};

                var xmlDoc = TestHelper.SerializeAndLoadXmlDocument(obj);

                xmlDoc.InnerXml.ShouldBe(
                    @"<?xml version=""1.0"" encoding=""iso-8859-1""?><HelloWorld1><HelloWorld>Hello World!</HelloWorld></HelloWorld1>");
            }
            catch (Exception ex)
            {
                CrestronConsole.PrintLine(ex.Message);
                CrestronConsole.PrintLine(ex.StackTrace);
                throw;
            }
        }

        [Fact]
        public void Hello_SimplDoc()
        {
            //Arrange
            var obj = new SimpleDoc
            {
                Attr1 = "Attribute 1", 
                Attr2 = "Attribute 2 as 3",
                ElementZero = "Element Zero (Order=3)",
                FirstElement = "First Element (Order=1)",
                SecondElement = "Second Element (Order=2)",
                NoNamespaceSet = 42
            };

            //Act
            var xmlDoc = TestHelper.SerializeAndLoadXmlDocument(obj);            

            //Assert
            xmlDoc.FirstChild.NodeType.ShouldBe(XmlNodeType.XmlDeclaration);            

            xmlDoc.LastChild.Name.ShouldBe("SimpleDoc");

            var nsManager = new XmlNamespaceManager(new NameTable());
            nsManager.AddNamespace("x1","http://crestron.eu/myNamespace");

            var rootNode = xmlDoc.SelectSingleNode(@"/x1:SimpleDoc",nsManager);            
            rootNode.LocalName.ShouldBe("SimpleDoc");
            rootNode.NamespaceURI.ShouldBe("http://crestron.eu/myNamespace");

            var firstAttr = rootNode.SelectSingleNode(@"@*[1]");
            firstAttr.LocalName.ShouldBe("Attr1"); //From the name of the property
            firstAttr.NamespaceURI.ShouldBe("http://crestron.eu/myNamespaceForAttributes");
            firstAttr.Value.ShouldBe("Attribute 1");

            var secondAttr = rootNode.SelectSingleNode(@"@*[2]");
            secondAttr.LocalName.ShouldBe("Attr3"); //From the XmlAttributeAttribute AtrributeName
            secondAttr.NamespaceURI.ShouldBe("http://crestron.eu/myNamespaceForAttributes");

            var firstChild = rootNode.SelectSingleNode(@"*[1]");
            firstChild.LocalName.ShouldBe("FirstElement");
            firstChild.NamespaceURI.ShouldBe("http://crestron.eu/myNamespace");
            firstChild.InnerText.ShouldBe("First Element (Order=1)");

            var secondChild = rootNode.SelectSingleNode(@"*[2]");
            secondChild.LocalName.ShouldBe("SecondElement");
            secondChild.NamespaceURI.ShouldBe("http://crestron.eu/myNamespace2");
            secondChild.InnerText.ShouldBe("Second Element (Order=2)");

            var thirdChild = rootNode.SelectSingleNode(@"*[3]");
            thirdChild.LocalName.ShouldBe("LastElement"); //From XmlElementAttribute ElementName
            thirdChild.NamespaceURI.ShouldBe("");
            thirdChild.InnerText.ShouldBe("Element Zero (Order=3)");

            var fourthChild = rootNode.SelectSingleNode(@"*[4]");
            fourthChild.LocalName.ShouldBe("NoNamespaceSet");
            fourthChild.NamespaceURI.ShouldBe("http://crestron.eu/myNamespace"); //Inherented from Root
            fourthChild.InnerText.ShouldBe("42");


        }

    }
}
