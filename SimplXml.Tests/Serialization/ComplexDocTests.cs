﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
using System;
using Crestron.EMEA.SimplUnit;
using Crestron.SimplSharp.CrestronIO;
using Crestron.SimplSharp.CrestronXml;
using SimplXml.Impl;
using SimplXml.Tests.TestObjects;

namespace SimplXml.Tests.Serialization
{
    public class ComplexDocTests
    {
        [Fact]
        public void EmpyComplexDoc()
        {
            //Arrange
            var complexDoc = new ComplexDoc();

            //Act
            var xmlDoc = SerializeAndLoadXmlDocument(complexDoc);

            //Assert
            xmlDoc.ShouldNotBeNull();
        }

        [Fact]
        public void SomeComplexDocContainingASimpleDoc()
        {
            //Arrange
            var complexDoc = new ComplexDoc
            {
                AnotherSimpleDoc = new SimpleDoc
                {
                    FirstElement = "Hello Simple Doc"
                }
            };

            //Act
            var xmlDoc = SerializeAndLoadXmlDocument(complexDoc);

            //Assert
            xmlDoc.ShouldNotBeNull();
            xmlDoc.InnerXml.Contains("Hello Simple Doc").ShouldBe(true);
        }

        [Fact]
        public void SomeComplexDocContainingAListOfNumbers()
        {
            //Arrange
            var complexDoc = new ComplexDoc
            {
                SomeNumbers = new[] { 1, 1, 2, 3, 5, 8, 13, 21 }
            };

            //Act
            var xmlDoc = SerializeAndLoadXmlDocument(complexDoc);

            //Assert           
            xmlDoc.ShouldNotBeNull();
            xmlDoc.InnerXml.Contains("<Int32>1</Int32>").ShouldBe(true);
            xmlDoc.InnerXml.Contains("<Int32>2</Int32>").ShouldBe(true);
            xmlDoc.InnerXml.Contains("<Int32>3</Int32>").ShouldBe(true);
            xmlDoc.InnerXml.Contains("<Int32>5</Int32>").ShouldBe(true);
            xmlDoc.InnerXml.Contains("<Int32>8</Int32>").ShouldBe(true);
            xmlDoc.InnerXml.Contains("<Int32>13</Int32>").ShouldBe(true);
            xmlDoc.InnerXml.Contains("<Int32>21</Int32>").ShouldBe(true);
        }
       
        [Fact]
        public void SomeComplexDocContainingAListOfStrings()
        {
            //Arrange
            var complexDoc = new ComplexDoc
            {
                SomeStrings = new[] { "hello", "world", "this", String.Empty }
            };

            //Act
            var xmlDoc = SerializeAndLoadXmlDocument(complexDoc);

            //Assert                       
            xmlDoc.ShouldNotBeNull();
            xmlDoc.InnerXml.Contains("<String>hello</String>").ShouldBe(true);
            xmlDoc.InnerXml.Contains("<String>world</String>").ShouldBe(true);
            xmlDoc.InnerXml.Contains("<String>this</String>").ShouldBe(true);
            xmlDoc.InnerXml.Contains("<String></String>").ShouldBe(true);
        }
                     
        [Fact]
        public void SomeComplexDocContainingAListOfSimpleDocs()
        {
            //Arrange
            var complexDoc = new ComplexDoc
            {
                SomeSimpleDocs = new[]
                {
                    new SimpleDoc
                    {
                        Attr1 = "not this one",
                        ElementZero = "Hello Simple Doc"
                    },
                    new SimpleDoc
                    {
                        Attr1 = "hello",
                        ElementZero = "world"
                    },
                    new SimpleDoc()
                }
            };

            //Act
            var xmlDoc = SerializeAndLoadXmlDocument(complexDoc);

            //Assert            
            xmlDoc.ShouldNotBeNull();
            xmlDoc.SelectSingleNode(@"//*[@*[local-name()='Attr1']='hello']/LastElement").InnerText.ShouldBe("world");
        }

        [Fact]
        public void SomeComplexDocRecursive()
        {
            //Arrange
            var complexDoc = new ComplexDoc
            {
               ComplexDocDescription = "Niveau 0",
               WhyNot = new ComplexDoc
               {
                   ComplexDocDescription = "Niveau 1",                   
                   WhyNot = new ComplexDoc
                   {
                       ComplexDocDescription = "Niveau 2",
                       WhyNot = new ComplexDoc
                       {
                           ComplexDocDescription = "Niveau 3",
                           SomeStrings = new []{"Hello World"}
                       }
                   }
               }
            };

            //Act
            var xmlDoc = SerializeAndLoadXmlDocument(complexDoc);

            //Assert            
            xmlDoc.ShouldNotBeNull();
            xmlDoc.SelectSingleNode(@"/ComplexDoc/WhyNot/WhyNot/WhyNot/SomeStrings/String[1]").InnerText.ShouldBe("Hello World");
        }

        private static XmlDocument SerializeAndLoadXmlDocument(object obj)
        {
            var xmlDoc = new XmlDocument();
            var stream = new MemoryStream();

            var sut = new XmlSerializerImp();

            sut.Serialize(stream, obj);

            stream.Flush();
            stream.Seek(0, SeekOrigin.Begin);

            xmlDoc.Load(stream);
            return xmlDoc;
        }
    }
}