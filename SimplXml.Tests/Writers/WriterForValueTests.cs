﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
using System.Text;
using Crestron.EMEA.SimplUnit;
using Crestron.SimplSharp.CrestronXml;
using SimplXml.Writers;

namespace SimplXml.Tests.Writers
{
    public class WriterForValueTests 
    {
        [Fact]
        public void WriteSimplElementWithValue()
        {
            var sb = new StringBuilder();
            var xmlWriter = new XmlWriter(sb);

            var elementWriter = new WriterForElement();
            elementWriter.Name = "Root";
            elementWriter.Namespace = "myNamespace";

            var valueWriter = new WriterForValue();
            valueWriter.Value = 5;

            elementWriter.Add(valueWriter);
            elementWriter.Write(xmlWriter);

            xmlWriter.Flush();
            xmlWriter.Close();

            sb.ToString().Contains("<Root xmlns=\"myNamespace\">5</Root>").ShouldBe(true);
        }
    }
}