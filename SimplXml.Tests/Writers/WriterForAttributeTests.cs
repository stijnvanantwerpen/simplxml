﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
using System.Text;
using Crestron.EMEA.SimplUnit;
using Crestron.SimplSharp.CrestronXml;
using SimplXml.Writers;

namespace SimplXml.Tests.Writers
{
    public class WriterForAttributeTests
    {
       
        [Fact]
        public void WriteElementWithAttributes()
        {
            var sb = new StringBuilder();
            var xmlWriter = new XmlWriter(sb);

            var elementWriter = new WriterForElement { Name = "Root", Namespace = "myNamespace" };
            var subWriter1 = new WriterForElement { Name = "Element1", Namespace = "myNamespace" };
            var subWriter2 = new WriterForElement { Name = "Element2", Namespace = "otherNamespace" };

            var attWriter11 = new WriterForAttribute { Name = "attr1", Namespace = "", Value = "Attr1" };
            var attWriter12 = new WriterForAttribute { Name = "attr2", Namespace = "myNamespace", Value = "Attr2" };
            var attWriter13 = new WriterForAttribute { Name = "attr3", Namespace = "otherNamespace", Value = "Attr3" };

            subWriter1.Add(attWriter11);
            subWriter1.Add(attWriter12);
            subWriter1.Add(attWriter13);

            elementWriter.Add(subWriter1);
            elementWriter.Add(subWriter2);

            elementWriter.Write(xmlWriter);

            xmlWriter.Flush();
            xmlWriter.Close();

            sb.ToString().Contains(
                "<Element1 attr1=\"Attr1\" p2:attr2=\"Attr2\" p3:attr3=\"Attr3\" xmlns:p3=\"otherNamespace\" xmlns:p2=\"myNamespace\" />")
                .ShouldBe(true);
        }

        [Fact]
        public void WriteAttributeWithNullValue()
        {
            //Arrange
            var sb = new StringBuilder();
            var xmlWriter = new XmlWriter(sb);

            var elementWriter = new WriterForElement { Name = "Root" };
            var attWriter1 = new WriterForAttribute { Name = "attr1", Namespace = ""};
          
            //Act
            elementWriter.Add(attWriter1);
            elementWriter.Write(xmlWriter);

            //Assert
            xmlWriter.Flush();
            xmlWriter.Close();

            sb.ToString().Contains(
                "<Root />")
                .ShouldBe(true);
        }
    }
}