﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
using System;
using System.Linq;
using Crestron.EMEA.SimplUnit;
using SimplXml.Tests.TestObjects;

namespace SimplXml.Tests.DeSerialization
{
    public class HelloComplexDocTests
    {
        [Fact]
        public void DeserializeComplexDoc_Empty()
        {
            var result = TestHelper.DeserilizeFrom<ComplexDoc>(ComplexDoc_Empty);

            result.ShouldNotBeNull();
            result.AnotherSimpleDoc.ShouldBeNull();

        }

        [Fact]
        public void DeserializeComplexDoc_SomeNumbers()
        {
            var result = TestHelper.DeserilizeFrom<ComplexDoc>(ComplexDoc_WithSomeNumbers);

            result.ShouldNotBeNull();
            result.SomeNumbers.ShouldNotBeNull();
            result.SomeNumbers.Count().ShouldBe(3);
            result.SomeNumbers.First().ShouldBe(1);
            result.SomeNumbers.Last().ShouldBe(3);
        }


        [Fact]
        public void DeserializeComplexDoc_SomeSimpleDocs()
        {
            var result = TestHelper.DeserilizeFrom<ComplexDoc>(ComplexDoc_SimpleDoc);

            result.ShouldNotBeNull();
            result.SomeSimpleDocs.ShouldNotBeNull();
            result.SomeSimpleDocs.Count().ShouldBe(3);
            result.SomeSimpleDocs.First().ShouldNotBeNull();
            result.SomeSimpleDocs.First().FirstElement.ShouldBeNull();
            result.SomeSimpleDocs.ElementAt(1).FirstElement.ShouldBe(String.Empty);
            result.SomeSimpleDocs.Last().FirstElement.ShouldBe("Hello World");
        }



        private const string ComplexDoc_Empty =
            @"<?xml version=""1.0"" encoding=""iso-8859-1""?>
              <ComplexDoc>
                <ComplexDocDescription />
                <SomeNumbers />
                <SomeStrings />
                <SomeSimpleDocs />
                <AnotherSimpleDoc />
                <WhyNot />
              </ComplexDoc>";

        private const string ComplexDoc_WithSomeNumbers =
           @"<?xml version=""1.0"" encoding=""iso-8859-1""?>
              <ComplexDoc>
                <ComplexDocDescription />
                <SomeNumbers>
                    <Int32>1</Int32>
                    <Int32>2</Int32>
                    <Int32>3</Int32>
                </SomeNumbers>
                <SomeStrings />
                <SomeSimpleDocs />
                <AnotherSimpleDoc />
                <WhyNot />
              </ComplexDoc>";

        private const string ComplexDoc_SimpleDoc =
            @"<?xml version=""1.0"" encoding=""iso-8859-1""?>
                <ComplexDoc>
                    <ComplexDocDescription />
                    <SomeNumbers />
                    <SomeStrings />
                    <SomeSimpleDocs>
                        <SimpleDoc xmlns=""http://crestron.eu/myNamespace"">
                            <FirstElement />
                            <SecondElement xmlns=""http://crestron.eu/myNamespace2"" />
                            <LastElement xmlns="""" />
                            <NoNamespaceSet>0</NoNamespaceSet>
                        </SimpleDoc>
                        <SimpleDoc xmlns=""http://crestron.eu/myNamespace"">
                            <FirstElement></FirstElement>
                            <SecondElement xmlns=""http://crestron.eu/myNamespace2"" />
                            <LastElement xmlns="""" />
                            <NoNamespaceSet>0</NoNamespaceSet>
                        </SimpleDoc>
                        <SimpleDoc xmlns=""http://crestron.eu/myNamespace"">
                            <FirstElement>Hello World</FirstElement>
                            <SecondElement xmlns=""http://crestron.eu/myNamespace2"" />
                            <LastElement xmlns="""" />
                            <NoNamespaceSet>0</NoNamespaceSet>
                        </SimpleDoc>
                    </SomeSimpleDocs>
                    <AnotherSimpleDoc />
                    <WhyNot />
                </ComplexDoc>";
    }
}