﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
using Crestron.EMEA.SimplUnit;
using SimplXml.Tests.TestObjects;

namespace SimplXml.Tests.DeSerialization
{
    public class HelloWorld2Tests
    {
        [Fact]
        public void DeSerilizeHello2()
        {
            var result = TestHelper.DeserilizeFrom<HelloWorld2>(
                "<?xml version=\"1.0\" encoding=\"utf-8\"?><HelloWorld2 number='123'><SomeText>Hello World!</SomeText></HelloWorld2>");
            
            result.ShouldNotBeNull();
            result.SomeText.ShouldBe("Hello World!");
            result.SomeNumber.ShouldBe(123);
        }

        [Fact]
        public void DeSerilizeHello2_optionalAttribute()
        {
            var result = TestHelper.DeserilizeFrom<HelloWorld2>(
                "<?xml version=\"1.0\" encoding=\"utf-8\"?><HelloWorld2><SomeText>Hello World!</SomeText></HelloWorld2>");

            result.ShouldNotBeNull();
            result.SomeText.ShouldBe("Hello World!");
            result.SomeNumber.ShouldBe(0);
        } 
    }
}