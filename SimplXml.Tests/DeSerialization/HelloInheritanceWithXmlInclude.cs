﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
using Crestron.EMEA.SimplUnit;
using SimplXml.Tests.TestObjects;

namespace SimplXml.Tests.DeSerialization
{
    public class HelloInheritanceWithXmlInclude
    {
        [Fact]
        public void DeSerialiseWithACat()
        {
            var result = TestHelper.DeserilizeFrom<HelloInheritance>(HelloInheritanceWithCat);
            
            result.ShouldNotBeNull();
            result.Animal.ShouldNotBeNull();
            var cat = result.Animal as MyCat;
            cat.ShouldNotBeNull();
            cat.Lives.ShouldBe(7);
            cat.Sound.ShouldBe("Miauw");

        }

        private const string HelloInheritanceWithCat =
            @"<?xml version=""1.0"" encoding=""iso-8859-1""?>
                <HelloInheritance 
                        xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" 
                        xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
                    <Animal xsi:type=""MyCat"">
                        <Lives>7</Lives>
                    </Animal>
                </HelloInheritance>";
    }
}