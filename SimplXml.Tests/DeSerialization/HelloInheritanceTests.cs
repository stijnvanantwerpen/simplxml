﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
using System.Linq;
using Crestron.EMEA.SimplUnit;
using SimplXml.Tests.TestObjects;

namespace SimplXml.Tests.DeSerialization
{
    public class HelloInheritanceTests
    {
        [Fact]
        public void DeserializeHelloInhertance_Single()
        {
            //Arrange
            const string xmlString =
                "<?xml version=\"1.0\" encoding=\"utf-8\"?><HelloInheritance xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'><Animal xsi:type='MyCat'><Lives>7</Lives></Animal></HelloInheritance>";

            //Act
            var result = TestHelper.DeserilizeFrom<HelloInheritance>(xmlString);

            result.Animal.ShouldNotBeNull();
            (result.Animal as MyCat).ShouldNotBeNull();
            ((MyCat) result.Animal).Lives.ShouldBe(7);
            result.Animal.Sound.ShouldBe("Miauw");
        }


        [Fact]
        public void DeserializeHelloInhertance_List()
        {
            //Arrange
            const string xmlString =
                @"<?xml version='1.0' encoding='utf-8'?>
                    <HelloInheritanceList
                            xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
                        <Animals>
                            <MyAnimal xsi:type='MyCat'>
                                <Lives>7</Lives>
                            </MyAnimal>
                            <MyAnimal xsi:type='MyDog'>
                                <Years>5</Years>
                            </MyAnimal>
                            <MyAnimal xsi:type='MyCat'>
                                <Lives>23</Lives>
                            </MyAnimal>
                            <MyAnimal xsi:type='MyCat'>
                                <Lives>5</Lives>
                            </MyAnimal>
                            <MyAnimal xsi:type='MyDog'>
                                <Years>23</Years>
                            </MyAnimal>
                        </Animals>
                    </HelloInheritanceList>";
            
            //Act
            var result = TestHelper.DeserilizeFrom<HelloInheritanceList>(xmlString);

            //Assert
            result.Animals.Count().ShouldBe(5);
            result.Animals.Count(a => a is MyCat).ShouldBe(3);
            result.Animals.Count(a => a is MyDog).ShouldBe(2);

            var cat = result.Animals.First() as MyCat;
            cat.ShouldNotBeNull();
            cat.Lives.ShouldBe(7);

            var dog = result.Animals.Last() as MyDog;
            dog.ShouldNotBeNull();
            dog.Years.ShouldBe(23);
        }
    }
}