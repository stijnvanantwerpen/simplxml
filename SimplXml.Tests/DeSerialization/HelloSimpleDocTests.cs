﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
using Crestron.EMEA.SimplUnit;
using SimplXml.Tests.TestObjects;

namespace SimplXml.Tests.DeSerialization
{
    public class HelloSimpleDocTests
    {
        [Fact]
        public void DeserializeSimpleDoc()
        {
            var simpleDoc = TestHelper.DeserilizeFrom<SimpleDoc>(SimpleDocXml);

            simpleDoc.ShouldNotBeNull();
            simpleDoc.Attr1.ShouldBe("Attribute 1");
            simpleDoc.Attr2.ShouldBe("Attribute 2 as 3");
            simpleDoc.FirstElement.ShouldBe("First Element (Order=1)");
            simpleDoc.SecondElement.ShouldBe("Second Element (Order=2)");
            simpleDoc.ElementZero.ShouldBe("Element Zero (Order=3)");
            simpleDoc.NoNamespaceSet.ShouldBe(42);
        }

        private const string SimpleDocXml =
            @"<?xml version=""1.0"" encoding=""iso-8859-1""?><SimpleDoc p1:Attr1=""Attribute 1"" p1:Attr3=""Attribute 2 as 3"" xmlns:p1=""http://crestron.eu/myNamespaceForAttributes"" xmlns=""http://crestron.eu/myNamespace""><FirstElement>First Element (Order=1)</FirstElement><SecondElement xmlns=""http://crestron.eu/myNamespace2"">Second Element (Order=2)</SecondElement><LastElement xmlns="""">Element Zero (Order=3)</LastElement><NoNamespaceSet>42</NoNamespaceSet></SimpleDoc>";
    }
}