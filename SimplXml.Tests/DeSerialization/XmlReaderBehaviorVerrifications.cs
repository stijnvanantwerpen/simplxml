﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
using Crestron.EMEA.SimplUnit;
using Crestron.SimplSharp.CrestronXml;

namespace SimplXml.Tests.DeSerialization
{
    public class XmlReaderBehaviorVerrifications
    {
        [Fact]
        public void ReadElementContentAs_AlsoPositions()
        {
            //This test exist to verify the behavior of ReadElementContentAsString()          
            const string xml = "<root><element1>value1</element1><element2>value2</element2></root>";
            var reader = new XmlReader(xml);
            reader.ReadToFollowing("root");
            reader.Read();
            reader.LocalName.ShouldBe("element1");
            var value = reader.ReadElementContentAsString();
            value.ShouldBe("value1");
            reader.LocalName.ShouldBe("element2");
        }
    }
}